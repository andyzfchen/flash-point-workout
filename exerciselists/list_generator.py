import pandas as pd

exercise_table = pd.read_csv("exerciselist_master.csv")
infos = exercise_table.columns[:4]
modes = exercise_table.columns[4:]

for mode in modes:
  temp_info = exercise_table[infos][exercise_table[mode]==1.0]
  temp_info.to_csv("exerciselist_%s.csv" % (mode.lower()), header=False, index=False)

temp_info = exercise_table[infos]
temp_info.to_csv("exerciselist_all.csv", header=False, index=False)
