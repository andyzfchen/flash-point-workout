// ####################################
// global webapp variables
var available_voices = window.speechSynthesis.getVoices();
var noSleep = new NoSleep();
var english_voices = [];
var english_voice = '';
var timerSpeed = 1000;

var exercises = "";
var currentexercise = "";
var nexercise = 0;
var nextexercise = "";
var gettingReady = true;
var goingToRest = false;
var switchingSides = false;
var resting = false;
var paused = false;
var started = false;
var timer = 0;
var iTimer = 0;
var nRep = 0;
var tRep = 0;
var nRpS = 0;
var tReady = 0;
var tBreak = 0;
var nInt = 0;
var iRep = -1;
var iInt = 0;
// ####################################

document.getElementById("nRep").onclick = function() {calculateTime()};
document.getElementById("tRep").onclick = function() {calculateTime()};
document.getElementById("nRpS").onclick = function() {calculateTime()};
document.getElementById("tReady").onclick = function() {calculateTime()};
document.getElementById("tBreak").onclick = function() {calculateTime()};

setTimeout(function() {initializeTTS();}, 50);
calculateTime();
setTime();

function customWorkout() {
  eMode = document.getElementById("eMode").value;
  fetch("exerciselists/exerciselist_"+eMode+".csv")
  .then( response => response.text() )
  .then( text => exercises = text )

  noSleep.enable();
  setTimeout(function() {initializeWorkout(eMode);}, 500);
}

function initializeWorkout(eMode) {
  exercises = $.csv.toArrays(exercises);
  nexercise = exercises.length;
  calculateTime();

  gettingReady = true;
  goingToRest = false;
  switchingSides = false;
  resting = false;
  started = true;
  timer = 11;
  iTimer = 0;
  nInt = nRep + Math.floor((nRep-1)/nRpS);
  iRep = -1;
  iInt = 0;

  // first exercise
  say("welcome to flash point workout");
  var durationText= "this "+eMode+" session is "+Math.floor(tTotal/60)+" minutes";
  if (tTotal/60 > 0.00001) {
    durationText += " and "+(tTotal-Math.floor(tTotal/60)*60)+" seconds";
  }
  say(durationText);
  iterateExercise();
  document.getElementById("status").innerHTML = "Welcome";
  document.getElementById("exercise").innerHTML = "&nbsp";
  if (exercises[iRep % nexercise][3] == 1) {
    document.getElementById("info").innerHTML = "Use a Stretch Band";
  }
  else {
    document.getElementById("info").innerHTML = "&nbsp";
  }
  document.getElementById("nextstatus").innerHTML = "First Exercise:";
  document.getElementById("nextexercise").innerHTML = currentexercise;
  say("first exercise");
  say(currentexercise);

  if (paused) {
    paused = false;
    document.getElementById("pause").innerHTML = "Pause";
    document.getElementById("pausestatus").innerHTML = "&nbsp";
  }
  else {
    document.getElementById("start").innerHTML = "Restart!";
    document.getElementById("pause").disabled = false;
  }

  document.getElementById("start").disabled = true;
  setTimeout(function() {timeLoop();}, timerSpeed);
}

function timeLoop() {
  // timer manager
  timer -= 1;

  if (iInt < nInt) {
    // next exercise
    if (timer == 10) {
      setNextExercise();
    }
    // switch sides
    else if (timer == Math.floor(tRep/2)) {
      switchSides();
    }
    // changes at end of rep
    else if (timer == 0) {
      resetTimer();
    }

    // countdown timer
    if (timer <= 3) {
      countdown();
    }

    // increment cumulative time
    if (!gettingReady && !switchingSides) {
      iTimer += 1;
    }

    document.getElementById("time").innerHTML = timer;
    document.getElementById('pb').style.width = Math.floor(iTimer/tTotal*1000)/10+'%';
    document.getElementById('pb').setAttribute("aria-valuenow", Math.floor(iTimer/tTotal*1000)/10);

    if (!paused) {
      setTimeout(function() {timeLoop();}, timerSpeed);
    }
  }
}

function finishWorkout() {
  timer = "&nbsp";
  document.getElementById("status").innerHTML = "Workout Completed";
  document.getElementById("exercise").innerHTML = "&nbsp";
  document.getElementById("time").innerHTML = "&nbsp";
  document.getElementById("nextstatus").innerHTML = "You Made It!";
  document.getElementById("nextexercise").innerHTML = "&nbsp";
  say("great job");
  say("you made it to the end");

  noSleep.disable();
  document.getElementById("start").disabled = false;
  document.getElementById("pause").disabled = true;
}

function resetTimer() {
  // continue an exercise after switching sides
  if (switchingSides) {
    document.getElementById("nextstatus").innerHTML = "Begin";
    timer = Math.floor(tRep/2);
    switchingSides = false;
    say("begin");
  }
  // reset to break time
  else if (goingToRest) {
    document.getElementById("status").innerHTML = "Break Time";
    document.getElementById("exercise").innerHTML = "relax and stretch";
    document.getElementById("nextstatus").innerHTML = "&nbsp";
    document.getElementById("nextexercise").innerHTML = "&nbsp";
    iInt += 1;
    timer = tBreak;
    gettingReady = false;
    goingToRest = false;
    resting = true;
    say("take a "+tBreak+" second break");
  }
  // reset to exercising or getting ready
  else {
    // reset to exercising
    if (gettingReady) {
      gettingReady = false;
      resting = false;
      document.getElementById("status").innerHTML = "Current Exercise:";
      document.getElementById("exercise").innerHTML = currentexercise;
      if (exercises[iRep % nexercise][1] == 1) {
        document.getElementById("info").innerHTML = "Switch Sides Halfway";
      }
      else {
        document.getElementById("info").innerHTML = "&nbsp";
      }
      document.getElementById("nextstatus").innerHTML = "Begin";
      document.getElementById("nextexercise").innerHTML = "&nbsp";
      timer = tRep;
      say("begin");
    }
    // reset to getting ready
    else {
      iterateExercise();
      iInt += 1;
      if (iInt < nInt) {
        gettingReady = true;
        resting = false;
        document.getElementById("status").innerHTML = "Current Exercise:";
        document.getElementById("exercise").innerHTML = currentexercise;
        if (exercises[iRep % nexercise][3] == 1) {
          document.getElementById("info").innerHTML = "Use a Stretch Band";
        }
        document.getElementById("nextstatus").innerHTML = "Get Ready";
        document.getElementById("nextexercise").innerHTML = "&nbsp";
        timer = tReady;
        say(currentexercise);
      }
      else {
        finishWorkout();
      }
    }
  }
}

function setNextExercise() {
  if (!gettingReady) {
    if ((iInt+1) == nInt) {
      document.getElementById("nextstatus").innerHTML = "Almost done!";
      document.getElementById("nextexercise").innerHTML = "&nbsp";
      say("almost done");
    }
    else if ((iInt+1) % (nRpS+1) == nRpS) {
      goingToRest = true;
      document.getElementById("nextstatus").innerHTML = "Next Exercise:";
      document.getElementById("nextexercise").innerHTML = "break time";
      say("next exercise");
      say("break time");
    }
    else {
      document.getElementById("nextstatus").innerHTML = "Next Exercise:";
      document.getElementById("nextexercise").innerHTML = nextexercise;
      say("next exercise");
      say(nextexercise);
    }
  }
}

function switchSides() {
  if (!resting && !gettingReady) {
    if (exercises[iRep % nexercise][1] == 1) {
      document.getElementById("nextstatus").innerHTML = "Switching Sides";
      document.getElementById("nextexercise").innerHTML = "Other Side";
      document.getElementById("info").innerHTML = "&nbsp";
      switchingSides = true;
      say("switch sides");
      timer = 5;
    }
  }
}

function countdown() {
  if (!gettingReady && !switchingSides) {
    say(timer);
  }
}

function iterateExercise() {
  iRep += 1;
  if ((iRep % nexercise) == 0) {
    shuffle(exercises);
  }

  currentexercise = exercises[iRep % nexercise][0];
  nextexercise = exercises[(iRep+1) % nexercise][0];
}

function pauseWorkout() {
  if (started) {
    if (!paused) {
      paused = true;
      document.getElementById("start").disabled = false;
      document.getElementById("pause").innerHTML = "Resume";
      document.getElementById("pausestatus").innerHTML = "Workout Paused";

      noSleep.disable();
    }
    else {
      paused = false;
      document.getElementById("start").disabled = true;
      document.getElementById("pause").innerHTML = "Pause";
      document.getElementById("pausestatus").innerHTML = "&nbsp";

      noSleep.enable();
      setTimeout(function() {timeLoop();}, timerSpeed);
    }
  }
}

function calculateTime() {
  nRep = parseInt(document.getElementById("nRep").value);
  tRep = parseInt(document.getElementById("tRep").value);
  nRpS = parseInt(document.getElementById("nRpS").value);
  tReady = parseInt(document.getElementById("tReady").value);
  tBreak = parseInt(document.getElementById("tBreak").value);

  tTotal = nRep*tRep + Math.floor((nRep-1)/nRpS)*tBreak;
  var tTotalText= Math.floor(tTotal/60)+" m "+(tTotal-Math.floor(tTotal/60)*60)+" s";
  document.getElementById("tTotal").innerHTML = tTotalText;
}

function setTime() {
  // nRep, tRep, nRpS, tReady, tBreak
  var timeStyles = [
    ["Insanity 10", 18, 30, 9, 5, 60],
    ["Sweat Out 20", 30, 30, 3, 5, 30],
    ["Slow Burn 30", 20, 60, 1, 5, 30],
    ["5min", 9, 30, 5, 5, 30],
    ["10min", 17, 30, 5, 5, 30],
    ["15min", 25, 30, 5, 5, 30],
    ["20min", 34, 30, 5, 5, 30],
    ["25min", 42, 30, 5, 5, 30],
    ["30min", 50, 30, 5, 5, 30]
  ];

  var timeStyle = timeStyles.find(obj => {
    return obj[0] === document.getElementById("tStyle").value
  });

  document.getElementById("nRep").value = timeStyle[1];
  document.getElementById("tRep").value = timeStyle[2];
  document.getElementById("nRpS").value = timeStyle[3];
  document.getElementById("tReady").value = timeStyle[4];
  document.getElementById("tBreak").value = timeStyle[5];

  calculateTime();
}
  
function initializeTTS() {
  // get all voices that browser offers
  available_voices = window.speechSynthesis.getVoices();

  var x = document.getElementById("voice");

  // find all voices with language locale "en-US" and chooses last one
  // if none then select the first voice
  for(var i=0; i<available_voices.length; i++) {
    if(available_voices[i].lang === 'en-US') {
      english_voices.push(available_voices[i]);

      var option = document.createElement("option");
      option.text = available_voices[i].name;
      x.add(option);
    }
  }
  if(english_voices.length == 0) {
    english_voice = available_voices[0];

    var option = document.createElement("option");
    option.text = "default";
    x.add(option);
  }
  else {
    setVoice();
  }
}

function setVoice() {
  english_voice = english_voices.find(obj => {
    return obj.name === document.getElementById("voice").value
  });
}

function say(text) {
  // new SpeechSynthesisUtterance object
  var utter = new SpeechSynthesisUtterance();
  utter.rate = 1;
  utter.pitch = 0.5;
  utter.text = text;
  utter.voice = english_voice;

  // speak
  window.speechSynthesis.speak(utter);
}

function shuffle(array) {
  var currentIndex = array.length, temporaryValue, randomIndex;

  // While there remain elements to shuffle...
  while (0 !== currentIndex) {

    // Pick a remaining element...
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex -= 1;

    // And swap it with the current element.
    temporaryValue = array[currentIndex];
    array[currentIndex] = array[randomIndex];
    array[randomIndex] = temporaryValue;
  }

  return array;
}
