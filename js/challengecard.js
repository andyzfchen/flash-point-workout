palettes = [
// [ bkg, box, title, details ]
[ '#89cff0', '#eeeeee', '#000000', '#777777' ],
[ '#f2aaaa', '#eeeeee', '#000000', '#777777' ],
[ '#b590ca', '#eeeeee', '#000000', '#777777' ],
[ '#a7e9af', '#eeeeee', '#000000', '#777777' ]
];
const npalette = palettes.length

const generalmodes = ['Stretching', 'Upper', 'Lower', 'Full', 'HIIT'];
const focusmodes = ['Arms', 'Shoulder', 'Chest', 'Back', 'Abs', 'Glutes', 'Legs'];
const challengemodes = ['20 Day Slow Burn', '5 Day Burn Out'];


const filetype = 'png';
const challenge = 1;
const palette = Math.floor(Math.random() * npalette);


const fs = require('fs');
const { createCanvas, loadImage } = require('canvas');

const width = 1000;
const height = 1000;

const canvas = createCanvas(width, height, filetype);
const context = canvas.getContext('2d');

// card border and bkg
context.fillStyle = palettes[palette][0];
context.fillRect(0, 0, width, height);
/*
context.strokeStyle = '#000000';
context.lineWidth = width/200*2;
context.beginPath();
context.moveTo(0, 0);
context.lineTo(0, width);
context.lineTo(height, width);
context.lineTo(height, 0);
context.lineTo(0, 0);
context.stroke();
*/

// card header
context.textAlign = 'center';
context.textBaseline = 'top';
loadImage('./resources/flashpointworkoutlogo.png').then(image => {
  context.drawImage(image, width*0.06, height*0.05, width*0.15, height*0.15);
  const buffer = canvas.toBuffer('image/'+filetype);
  fs.writeFileSync('./test/challengecard.'+filetype, buffer);
})

const title = 'Flash Point Challenge';
context.font = 'bold 40pt Impact';
context.fillStyle = palettes[palette][2];
context.fillText(title, width*0.58, height*0.06);
context.fillText(challengemodes[challenge], width*0.58, height*0.14);

// card footer
context.textAlign = 'right';
context.textBaseline = 'bottom';
context.fillStyle = palettes[palette][2];
context.font = 'bold 14pt Impact';
context.fillText('flashpointworkout.com', width*0.98, height*0.99);

// x, y from top left corner
// x, y, width, height

// grid boxes
if (challenge==0) {
  var i, j;
  const times = [8, 10, 13, 15];
  const rows = 4;
  const cols = 5;
  const colbuffer = width*0.02
  const borderwidth = width*0.005;
  const rowupper = height*0.25;
  const rowlower = height*0.96;
  const rowwidth = (rowlower-rowupper)/rows;
  const colwidth = width*0.96/cols;

  for (i=0; i<rows; i++) {
    shuffle(generalmodes);

    for (j=0; j<cols; j++) {
      // box
      context.fillStyle = palettes[palette][1];
      context.fillRect(colbuffer+j*colwidth+borderwidth, rowupper+i*rowwidth+borderwidth, colwidth-2*borderwidth, rowwidth-2*borderwidth);

      // day number
      context.textAlign = 'right';
      context.textBaseline = 'bottom';
      context.fillStyle = palettes[palette][0];
      context.font = 'bold 30pt Impact';
      context.fillText(i*cols+j+1, colbuffer+j*colwidth+colwidth-width*0.01, rowupper+i*rowwidth+rowwidth-height*0.005);

      // day details
      context.textAlign = 'center';
      context.textBaseline = 'top';
      context.fillStyle = palettes[palette][3];
      context.font = 'bold 20pt Impact';
      context.fillText(5+5*i+' Mins', colbuffer+j*colwidth+colwidth/2, rowupper+i*rowwidth+height*0.02);
      context.fillStyle = palettes[palette][3];
      context.font = 'bold 25pt Impact';
      context.fillText(generalmodes[j], colbuffer+j*colwidth+colwidth/2, rowupper+i*rowwidth+height*0.08);
    }
  }
}

else if (challenge==1) {
  var i, j;
  const times = [10, 15, 10];
  const rows = 5;
  const cols = 5;
  const colbuffer = width*0.02
  const borderwidth = width*0.005;
  const rowupper = height*0.25;
  const rowlower = height*0.96;
  const rowwidth = (rowlower-rowupper)/rows;
  const colwidth = width*0.96/cols;

  for (j=0; j<cols; j++) {
    shuffle(focusmodes);

    // box
    context.fillStyle = palettes[palette][1];
    context.fillRect(colbuffer+j*colwidth+borderwidth, rowupper+borderwidth, colwidth-2*borderwidth, rowlower-rowupper-2*borderwidth);

    // day number
    context.textAlign = 'right';
    context.textBaseline = 'bottom';
    context.fillStyle = palettes[palette][0];
    context.font = 'bold 30pt Impact';
    context.fillText(j+1, colbuffer+j*colwidth+colwidth-width*0.01, rowlower-height*0.005);

    for (i=0; i<rows; i++) {
      // day details
      if ((i%2)==0) {
        context.textAlign = 'center';
        context.textBaseline = 'top';

        context.fillStyle = palettes[palette][3];
        context.font = 'bold 20pt Impact';
        context.fillText(times[i/2]+' Mins', colbuffer+j*colwidth+colwidth/2, rowupper+i*rowwidth+height*0.03);
        context.fillStyle = palettes[palette][3];
        context.font = 'bold 25pt Impact';
        context.fillText(focusmodes[i], colbuffer+j*colwidth+colwidth/2, rowupper+i*rowwidth+height*0.07);
      }
      else if ((i%2)==1) {
        context.textAlign = 'center';
        context.textBaseline = 'top';

        context.fillStyle = palettes[palette][0];
        context.font = 'bold 30pt Impact';
        context.fillText('and', colbuffer+j*colwidth+colwidth/2, rowupper+i*rowwidth+rowwidth/3);
      }
    }
  }
}

function shuffle(array) {
  var currentIndex = array.length, temporaryValue, randomIndex;

  // While there remain elements to shuffle...
  while (0 !== currentIndex) {

    // Pick a remaining element...
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex -= 1;

    // And swap it with the current element.
    temporaryValue = array[currentIndex];
    array[currentIndex] = array[randomIndex];
    array[randomIndex] = temporaryValue;
  }

  return array;
}
